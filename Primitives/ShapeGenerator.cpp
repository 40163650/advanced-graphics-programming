#include "ShapeGenerator.h"

XMFLOAT3 XMFloat3Normalize(XMFLOAT3 v)
{
	float length = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);

	float x = v.x / length;
	float y = v.y / length;
	float z = v.z / length;

	return XMFLOAT3(x, y, z);
}

XMFLOAT3 smallest(XMFLOAT3 a, XMFLOAT3 b)
{
	float lengthA = (a.x * a.x + a.y * a.y + a.z * a.z);
	float lengthB = (b.x * b.x + b.y * b.y + b.z * b.z);

	if (min(lengthA, lengthB) == lengthA)
		return a;
	return b;
}

XMFLOAT3 largest(XMFLOAT3 a, XMFLOAT3 b)
{
	float lengthA = (a.x * a.x + a.y * a.y + a.z * a.z);
	float lengthB = (b.x * b.x + b.y * b.y + b.z * b.z);

	if (max(lengthA, lengthB) == lengthA)
		return a;
	return b;
}

/* Basic - Creates a torus at a fixed position and scale */
void ShapeGenerator::CreateTorus(GeometryGenerator::MeshData &meshData)
{
	
	GeometryGenerator::Vertex v[16];

	v[0] = GeometryGenerator::Vertex( 3, 1,  3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[1] = GeometryGenerator::Vertex(-3, 1,  3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[2] = GeometryGenerator::Vertex( 1, 1,  1, 0, 0, 0, 0, 0, 0, 0, 0);
	v[3] = GeometryGenerator::Vertex(-1, 1,  1, 0, 0, 0, 0, 0, 0, 0, 0);
	v[4] = GeometryGenerator::Vertex(-3, 1, -3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[5] = GeometryGenerator::Vertex(-1, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0);
	v[6] = GeometryGenerator::Vertex( 1, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0);
	v[7] = GeometryGenerator::Vertex( 3, 1, -3, 0, 0, 0, 0, 0, 0, 0, 0);

	v[8] = GeometryGenerator::Vertex( 3, 0, -3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[9] = GeometryGenerator::Vertex(-3, 0, -3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[10]= GeometryGenerator::Vertex(-3, 0,  3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[11]= GeometryGenerator::Vertex( 3, 0,  3, 0, 0, 0, 0, 0, 0, 0, 0);
	v[12]= GeometryGenerator::Vertex( 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0); // B
	v[13]= GeometryGenerator::Vertex(-1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0); // C
	v[14]= GeometryGenerator::Vertex(-1, 0,  1, 0, 0, 0, 0, 0, 0, 0, 0); // D
	v[15]= GeometryGenerator::Vertex( 1, 0,  1, 0, 0, 0, 0, 0, 0, 0, 0); // A

	UINT i[96] = {
		// Top Face - Clockwise winding order
		1, 3, 4,
		3, 5, 4,
		4, 5, 7,
		5, 6, 7,
		7, 6, 0,
		6, 2, 0,
		2, 1, 0,
		2, 3, 1,
	
		// Front Face - Negative X
		10, 1, 9,
		4, 9, 1,

		// Right Face - Negative Z
		9, 4, 8,
		4, 7, 8,

		// Back Right - Positive X
		7, 11, 8,
		7, 0, 11,

		// Back Left - Positive Z
		0, 10, 11,
		0, 1, 10,

		// Inner Back Right - Positive X
		15, 2, 12,
		2, 6, 12,

		// Inner Back Left - Positive Z
		3, 2, 14,
		14, 2, 15,

		// Inner Front Left - Negative X
		14, 5, 3,
		13, 5, 14,

		// Inner Front Right - Negative Z
		5, 13, 12,
		12, 6, 5,

		// Bottom Side
		9, 13, 10,
		13, 14, 10,
		9, 8, 13,
		8, 12, 13,
		8, 11, 15,
		12, 8, 15,
		15, 11, 10,
		15, 10, 14
	};
	meshData.Vertices.assign(&v[0], &v[16]);

	meshData.Indices.assign(&i[0], &i[96]);

}

/* Intermediate - Adjustable Torus */
void ShapeGenerator::CreateTorus(const UINT stackCount, const UINT sliceCount, const float ringRadius, const float outerRadius, GeometryGenerator::MeshData &meshData)
{
	meshData.Vertices.clear();
	meshData.Indices.clear();

	// Declare required buffers - positions
	std::vector<XMFLOAT3> positions;

	// The minimal and maximal points
	XMFLOAT3 minimal(0.0f, 0.0f, 0.0f);
	XMFLOAT3 maximal(0.0f, 0.0f, 0.0f);

	// Calculate delta for stack and slice
	auto delta_stack = 2.0f * XM_PI / static_cast<float>(stackCount);
	auto delta_slice = 2.0f * XM_PI / static_cast<float>(sliceCount);

	// Calculate outer circumference
	auto outer_circ = 2.0f * XM_PI * outerRadius;
	auto ring_circ = 2.0f * XM_PI * ringRadius;

	// Create stacks for torus
	for (UINT i = 0; i < stackCount; ++i)
	{
		// Working values for stack
		auto a0 = i * delta_stack;
		auto a1 = a0 + delta_stack;
		std::array<GeometryGenerator::Vertex, 4> verts;
		std::array<XMFLOAT3, 4> norms;
		std::array<XMFLOAT2, 4> coords;

		// Iterate through each slice for stack
		for (UINT j = 0; j <= sliceCount; ++j)
		{
			// Working values for slice
			auto b = j * delta_slice;
			auto c = cos(j * delta_slice) * ringRadius;
			auto r = c + outerRadius;

			// Vertex 0
			verts[0] = GeometryGenerator::Vertex(sin(a0) * r, sin(j * delta_slice) * ringRadius, cos(a0) * r, 0,0,0,0,0,0,0,0);
			// Vertex 1
			verts[1] = GeometryGenerator::Vertex(sin(a1) * r, sin(j * delta_slice) * ringRadius, cos(a1) * r, 0, 0, 0, 0, 0, 0, 0, 0);
			// Vertex 2
			c = cos((j + 1) * delta_slice) * ringRadius;
			r = c + outerRadius;
			verts[2] = GeometryGenerator::Vertex(sin(a0) * r, sin((j + 1) * delta_slice) * ringRadius, cos(a0) * r, 0, 0, 0, 0, 0, 0, 0, 0);
			// Vertex 3
			verts[3] = GeometryGenerator::Vertex(sin(a1) * r, sin((j + 1) * delta_slice) * ringRadius, cos(a1) * r, 0, 0, 0, 0, 0, 0, 0, 0);

			meshData.Vertices.push_back(verts[0]);
			meshData.Vertices.push_back(verts[1]);
			meshData.Vertices.push_back(verts[2]);

			meshData.Vertices.push_back(verts[1]);
			meshData.Vertices.push_back(verts[3]);
			meshData.Vertices.push_back(verts[2]);
		}
	}

	for (int i = 0; i < meshData.Vertices.size(); i++)
	{
		meshData.Indices.push_back(i);
	}
}