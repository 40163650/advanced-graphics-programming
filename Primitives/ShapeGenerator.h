#pragma once
#include <GeometryGenerator.h>
#include "MathHelper.h"
#include <array>

class ShapeGenerator {
public:
	void CreateTorus(GeometryGenerator::MeshData &meshData);
	void CreateTorus(const UINT stackCount, const UINT sliceCount, const float ringRadius, const float outerRadius, GeometryGenerator::MeshData &meshData);

};
