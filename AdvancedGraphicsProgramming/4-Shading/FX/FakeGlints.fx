#include "LightHelper.fx"

#define time _Time
#define FAR 30.
#define ITR 60

#define PRIMARY_INTENSITY 1.3
#define PRIMARY_CONCENTRATION 12.
#define SECONDARY_INTENSITY 5.
#define SECONDARY_CONCENTRATION 0.9

float3    _ScreenParams;                   // viewport resolution (in pixels)
float     _Time;                           // shader playback time (in seconds)
//uniform float     iTimeDelta;            // render time (in seconds)
//uniform int       iFrame;                // shader playback frame
//uniform float     iChannelTime[4];       // channel playback time (in seconds)
//uniform float3    iChannelResolution[4]; // channel resolution (in pixels)
float4    iMouse;                          // mouse pixel coords. xy: current (if MLB down), zw: click
Texture2D iChannel0;                       // input channel. XX = 2D/Cube
//uniform float4    iDate;                 // (year, month, day, time in seconds)
//uniform float     iSampleRate;           // sound sample rate (i.e., 44100)

SamplerState g_samPoint
{
	Filter = MIN_MAG_MIP_POINT;
	AddressU = Wrap;
	AddressV = Wrap;
};

float2x2 mm2(in float a)
{
	float c = cos(a), s = sin(a);
	return float2x2(c, -s, s, c); 
}

float2 Noise(in float3 x)
{
	float3 p = floor(x);
	float3 f = frac(x);
	f = f * f * (3.0 - 2.0 * f);
	float2 uv = (p.xy + float2(37.0, 17.0) * p.z) + f.xy;
	// iChannel refers to the input texture
	float4 rg = iChannel0.SampleLevel(g_samPoint, (uv + 0.5) / 256.0, 0.0);
	return lerp(rg.yw, rg.xz, f.z);
}

float length16(float3 p)
{
	p = p * p; p = p * p; p = p * p; p = p * p;
	return pow(p.x + p.y + p.z, 1.0 / 16.0);
}

float map(float3 p)
{
	float d = length16(p) - 0.5;
	p.xy *= .7;
	return lerp(d, length(p) - 1.5, 0.5);
}

//Based on TekF's "Anisotropic Highlights" (https://www.shadertoy.com/view/XdB3DG)
float3 shade(float3 pos, float3 rd, float3 normal, float3 ligt)
{
	float3 lcol = float3(.48, .45, .9);
	float nl = dot(normal, ligt);
	float3 light = lcol * max(.0, nl) * 1.5;
	//light += lerp(float3(.07,.07,.07), float3(.15), (-normal.y + 1.0));	
	float3 h = normalize(ligt - rd);
	float3 rf = reflect(rd, normal);

	float3 coord = pos * .5;
	coord.xy = coord.xy * .7071 + coord.yx * .7071 * float2(1, -1);
	coord.xz = coord.xz * .7071 + coord.zx * .7071 * float2(1, -1);
	float3 coord2 = coord;

	//displacement of the noise grabs to create the glinting effect
#if 1    
	float3 ww = fwidth(pos);
	coord.xy  -= h.xz * 20. * ww.xy;
	coord.xz  -= h.xy * 20. * ww.xz;
	coord2.xy -= h.xy * 5.  * ww.xy;
	coord2.xz -= h.xz * 5.  * ww.xz;
#endif

	//first layer (inner glints)
	float pw = .21 * ((_ScreenParams.x));
	float3 aniso = float3(Noise(coord * pw), Noise(coord.yzx * pw).x) * 2.0 - 1.0;
	aniso -= normal * dot(aniso, normal);
	float anisotropy = min(1., length(aniso));
	aniso /= anisotropy;
	anisotropy = .55;
	float ah = abs(dot(h, aniso));
	float nh = abs(dot(normal, h));
	float q = exp2((1.1 - anisotropy) * 3.5);
	nh = pow(nh, q * PRIMARY_CONCENTRATION);
	nh *= pow(1. - ah * anisotropy, 10.0);
	float3 glints = lcol * nh * exp2((1.2 - anisotropy) * PRIMARY_INTENSITY);
	glints *= smoothstep(.0, .5, nl);

	//second layer (outer glints)
	pw = .145*((_ScreenParams.x));
	float3 aniso2 = float3(Noise(coord2 * pw), Noise(coord2.yzx * pw).x)  *2.0 - 1.0;
	anisotropy = .6;
	float ah2 = abs(dot(h, aniso2));
	float q2 = exp2((.1 - anisotropy) * 3.5);
	float nh2 = pow(nh, q2 * SECONDARY_CONCENTRATION);
	nh2 *= pow(1. - ah2 * anisotropy, 150.0);
	float3 glints2 = lcol * nh2 * ((1. - anisotropy) * SECONDARY_INTENSITY);
	glints2 *= smoothstep(.0, .4, nl);

	float3 reflection = float3(0,0,0);

	float frnl = pow(1.0 + dot(normal, rd), 5.0);
	frnl = lerp(.0, .25, frnl);

	return lerp(light * float3(.3,.3,.3), reflection, frnl)
		+ glints
		+ glints2
		+ reflection * 0.015 * (clamp(nl, 0., 1.))
		+ reflection * 0.005
		+ lcol * 0.1;
}

float march(float3 pos, float3 ray)
{
	float d = 0.;
	float h;
	for (int i = 0; i < ITR; i++)
	{
		h = map(pos + d * ray);
		if (h < .005 || d > FAR)
			break;
		d = d + h;
	}

	if (d > FAR)
		return 0.;
	else
		return d;
}

float3 normal(in float3 p, in float3 rd)
{
	float2 e = float2(-1., 1.)*0.01;
	float3 n = (e.yxx * map(p + e.yxx)
		+ e.xxy * map(p + e.xxy)
		+ e.xyx * map(p + e.xyx)
		+ e.yyy * map(p + e.yyy));

	//from TekF (error checking)
	float gdr = dot(n, rd);
	n -= max(.0, gdr) * rd;
	return normalize(n);
}

float4 mainImage(float4 fragCoord : SV_POSITION) : SV_Target
{
	//setup
	float2 p = fragCoord.xy / _ScreenParams.xy - 0.5;
	p.x *= _ScreenParams.x / _ScreenParams.y;
	float2 um = iMouse.xy / _ScreenParams.xy - .5;
	um.x *= _ScreenParams.x / _ScreenParams.y;

	//camera
	float3 ro = float3(0., 0., -5.);
	float3 rd = normalize(float3(p, 1.5));
	float2x2 mx = mm2(sin(time * .24) * 0.5 + um.x * 6.);
	float2x2 my = mm2(sin(time * 0.15) * 1.5 + um.y * 6. + 3.5);
	ro.xz = mul(ro.xz,  mx); rd.xz = mul(rd.xz, mx);
	ro.xy = mul(ro.xy, my); rd.xy = mul(rd.xy, my);

	float3 col = float3(0, 0, 0);

	float t = march(ro, rd);
	if (t > .0)
	{
		float3 ligt = normalize(float3(1., 1, -.2));
		float3 p = ro + rd * t;
		float3 n = normal(p, rd);
		col = shade(p, rd, n, ligt);
	}
	return float4(pow(col, float3(.85, .85, .85)), 1);
}

struct VertexOut
{
	float4 PosH    : SV_POSITION;
	float3 PosW    : POSITION;
	float3 NormalW : NORMAL;
};


float4 PS(VertexOut pin) : SV_Target
{
	return float4(0,0,0,1);
}

technique11 GlintTech
{
	pass P0
	{
		SetPixelShader(CompileShader(ps_5_0, mainImage()));
	}
}

technique11 main
{
	pass P0
	{
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}